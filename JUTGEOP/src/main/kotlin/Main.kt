import DAO.DAO.Companion.connection
import DAO.QuestionsDao
import DAO.QuestionsDaoInterface
import DAO.TeachersDao
import DAO.UsersDao

import Model.*
import UI.ConsoleInputs
import java.sql.*
import java.util.*
import kotlin.system.exitProcess

//COLORS
const val redColor = "\u001b[31m"
const val yellowColor = "\u001B[33m"
const val greenColor = "\u001B[32m"
const val blueColor = "\u001B[36m"
const val reset = "\u001b[0m"
const val softGreenColor = "\u001B[38;5;70m"

fun main() {
    val userDAO = UsersDao()
    val teacherDAO = TeachersDao()
    val questionsDAO = QuestionsDao()

    val scan = Scanner(System.`in`)
    var letsPlay = true
    var teacherMode = false

    //INITIALIZE GAME
    println("Benvingut a" + yellowColor + " JOC DE PROVES" + reset)
    print(greenColor+" ▶ INTRODUEIX EL NOM DE JUGADOR: "+reset)
    val inputIGN = scan.next()
    var IGNexists = false
    print(greenColor+" ▶ INTRODUEIX LA CONTRASEÑA: "+reset)
    var inputPassword = scan.next()
    var passCorrect = false

    var userExists = false
    var notPlayedYet = false

    var IDPerson:Int = 0

    val users = userDAO.getAllUsers()
    val teachers = teacherDAO.getAllTeachers()
    val questionsAll = questionsDAO.getAllQuestions()

    //LOGIN SYSTEM
    //CHECK IF NAME AND PASS EXISTS (IN USERS TABLE)
    for (i in 0 until users!!.size){
        if(inputIGN == users[i].name){
            if (inputPassword == users[i].password){
                println("$greenColor[LOGGED CORRECTED]$reset")
                notPlayedYet = true
                userExists = true
                teacherMode = false
                passCorrect = true
                break
            }
            while (passCorrect == false){
                println("$redColor[ERROR]$greenColor Has introduit malament la contraseña, torna a introduirla$reset")
                inputPassword = scan.next()
                if (inputPassword == users[i].password){
                    println("$greenColor[LOGGED CORRECTED]$reset")
                    notPlayedYet = true
                    userExists = true
                    teacherMode = false
                    passCorrect = true
                    break
                }

            }
        }
    }

    //CHECK IF NAME AND PASS EXISTS (IN TEACHERS TABLE)
    for (i in 0 until teachers!!.size){
        if(inputIGN == teachers[i].name){
            if (inputPassword == teachers[i].password){
                println("$greenColor[LOGGED CORRECTED] Hi Teacher!$reset")
                teacherMode = true
                IDPerson = i
                break
            }
            while (passCorrect == false){
                println("$redColor[ERROR]$greenColor Has introduit malament la contraseña, torna a introduirla$reset")
                inputPassword = scan.next()
                if (inputPassword == users[i].password){
                    println("$greenColor[LOGGED CORRECTED]$reset")
                    notPlayedYet = true
                    userExists = true
                    teacherMode = true
                    passCorrect = true
                    IDPerson = i
                    break
                }
                break
            }
        }
    }

    if (userExists == false){
        val numberOfUsers = users.size
        userDAO.addNewUser(inputIGN,inputPassword, numberOfUsers+1)

        letsPlay = true
    }

    while (letsPlay){
        var goodAnswer = 0
        var badAnswer = false
        when(teacherMode){
            //TEACHER MODE
            true -> {
                val nameSavedOnDB = inputIGN
                ConsoleInputs().teacherPanel(nameSavedOnDB)
                println(" ▶ Escriu el número de l'acció que vols " + greenColor + "portar a terme"+ reset)
                print("$inputIGN ➜ ")
                when(scan.nextInt()){
                    1 ->{
                        println(" ▶ A continuació set demanaràn les dades per afegir el problema a la llista de problemes"+ reset)
                        println(" ▶ Recorda que a part de la pregunta inicial hauràs d'afegir:\n 2 enunciats d'ajuda, on s'aclarin requisits i petits detalls \n i una llista a pastebin"+ reset)
                        Teacher(IDPerson, inputIGN, inputPassword).addProblem(scan,inputIGN)
                    }
                    2 ->{
                        println("▶ Introdueix el nom de l'alumne per revisar el seu report "+ reset)
                        println(redColor+" [ATENCIÓ]"+reset+" Recorda que el nom de l'usuari haurà d'estar a la BD")
                        print(inputIGN + " ➜ ")
                        val alumName = scan.next()
                        Teacher(IDPerson, inputIGN, inputPassword).alumReport(alumName, questionsAll!!)
                        var teacherInputContine:String
                        do {
                            println("Vols continuar? "+ reset)
                            println("▶ Escriu "+yellowColor+"SI"+reset+" per continuar"+reset)
                            teacherInputContine = scan.next().uppercase()
                        }while (teacherInputContine != "SI")
                    }
                }
            }
            //NORMAL MODE
            false ->{
                do {
                    val questions = questionsDAO.getAllQuestions()
                    val questionsDataUsers = questionsDAO.getAllQuestionsDataFromUser(inputIGN)
                    ConsoleInputs().initialPanel()
                    println(" ▶ Escriu el número de l'acció que vols " + greenColor + "portar a terme"+ reset)
                    print(inputIGN + " ➜ ")
                    val inputActionPanel = scan.nextInt()
                    println()
                    when(inputActionPanel){
                        1 ->{
                            ConsoleInputs().panelQuestions()
                            println(greenColor+"▶ Escriu el " + yellowColor + "NÚMERO " + greenColor + "per seleccionar la pregunta o bé per sortir" + reset)
                            print(inputIGN + " ➜ ")
                            val inputAction = scan.nextInt()
                            val actionToDo = inputAction-1
                            if (inputAction != 9){
                                println()
                                ConsoleInputs().problema(actionToDo,questions!!)
                                ConsoleInputs().pregunta()
                                println()
                                print(inputIGN + " ➜ ")
                                val inputResolveOrNot = scan.nextInt()
                                println(inputResolveOrNot)
                                var state = false
                                var trys = 0
                                var formatedAnswer:String
                                var resultMutableList = MutableList(4){""}
                                var star = ""
                                if (inputResolveOrNot == 1){
                                    ConsoleInputs().infoProblemes(actionToDo,questions)
                                    println(softGreenColor+" ▶ Envia la teva resposta ➜"+reset)
                                    var inputProblemAnswer:String
                                    do {
                                        inputProblemAnswer = scan.nextLine()
                                    }while (inputProblemAnswer == "")

                                    //WE FORMAT OUR ANSWER, REMOVING SPACES FOR DASHES
                                    formatedAnswer = Judge().formatAnswer(inputProblemAnswer)

                                    while (formatedAnswer != questions[actionToDo].correct_answer && trys != 4){
                                        if (trys < 4){
                                            println(greenColor+"Et queden "+redColor+"${4-trys}"+greenColor+" intents")
                                        }
                                        println(redColor+ "[ERROR]"+softGreenColor+" ▶ Envia un altre cop la resposta ➜"+reset)
                                        inputProblemAnswer = scan.nextLine()
                                        formatedAnswer = Judge().formatAnswer(inputProblemAnswer)
                                        trys++
                                    }
                                    if (trys == 4){
                                        resultMutableList = Judge().addStarsIfNotSolved(actionToDo+1,resultMutableList)
                                        println("Has perdut")
                                        star = "☆"
                                    }
                                    else if (formatedAnswer == questions[actionToDo].correct_answer){
                                        resultMutableList = Judge().addStarsIfSolved(actionToDo+1,resultMutableList)
                                        goodAnswer++
                                        star = "★"
                                        state = true
                                    }
                                } else{
                                    resultMutableList = Judge().addStarsIfNotSolved(actionToDo+1,resultMutableList)
                                    badAnswer = true
                                    star = "☆"
                                }
                                ConsoleInputs().statsProblema(actionToDo, state, trys+1)
                                notPlayedYet = true

                                var points:Int
                                if (inputResolveOrNot == 1){
                                    if (badAnswer){
                                        points = 0
                                        badAnswer = false
                                    }else{
                                        points = Judge().puntuation(trys+1)
                                    }
                                }else{
                                    points = 0
                                }
                                userDAO.updateUserProblem(inputAction+1,points,star)
//                                questionSelected = actionToDo
                            }
                        }
                        2 ->{
                            if (notPlayedYet == false){
                                println()
                                println(redColor+ "[ATENCIÓ]"+softGreenColor+" Abans de veure el teu historial, hauràs de jugar, ja que aquest usuari no te registres anteriors")
                                println("Ho has entès?")
                                do {
                                    println(reset+"Escriu SI per continuar")
                                    val inputUserYes = scan.next().uppercase()
                                }while (inputUserYes != "SI")
                            }else{
                                ConsoleInputs().panelHistoric(questions!!, questionsDataUsers!!)
                                do {
                                    println("Aquí tens el teu històric \nEscriu SI per continuar")
                                    val inputUserYes = scan.next().uppercase()
                                }while (inputUserYes != "SI")
                            }
                        }
                        3 ->{
                            ConsoleInputs().menuPanel()
                            do {
                                println(softGreenColor+"▶ Si has acabat amb el menu, escriu " + yellowColor + "ACABAR " + softGreenColor + "per tornar al submenú" + reset)
                                print(inputIGN + " ➜ ")
                                val inputHelpFinished = scan.next().uppercase()
                                println()
                            }while (inputHelpFinished != "ACABAR")
                        }
                        4 ->{
                            var game = true
                            var turn:Int
                            var resultMutableList = MutableList(4){""}
                            println()
                            println(greenColor+"JOC DE PROVES | Camí al torneig"+reset)
                            while (game){
                                turn = 1
                                val problemsSolvedState = questionsDataUsers

                                for (problem in problemsSolvedState!!){
                                    if (problem.estate_question == "★" || problem.estate_question == "☆"){
                                        turn++
                                    }
                                    else {
                                    }
                                }

                                questions!![(turn-1)]
                                ConsoleInputs().pregunta()
                                print(inputIGN + " ➜ ")
                                val inputResolveOrNot = scan.nextInt()
                                println(inputResolveOrNot)
                                var state = false
                                var trys = 0
                                var star = ""
                                var formatedAnswer:String
                                if (inputResolveOrNot == 1){
                                    ConsoleInputs().infoProblemes(turn-1,questions)
                                    trys=1
                                    println(softGreenColor+" ▶ Envia la teva resposta ➜"+reset)
                                    var inputProblemAnswer:String
                                    do {
                                        inputProblemAnswer = scan.nextLine()
                                    }while (inputProblemAnswer == "")
                                    formatedAnswer = Judge().formatAnswer(inputProblemAnswer)//WE FORMAT OUR ANSWER, REMOVING SPACES FOR DASHES
                                    while (formatedAnswer != questions[turn-1].correct_answer && trys != 4){
                                        if (trys < 4){
                                            println(greenColor+"Et queden "+redColor+"${4-trys}"+greenColor+" intents")
                                        }
                                        println(redColor+ "[ERROR]"+softGreenColor+" ▶ Envia un altre cop la resposta ➜"+reset)
                                        inputProblemAnswer = scan.nextLine()
                                        formatedAnswer = Judge().formatAnswer(inputProblemAnswer)
                                        trys++
                                    }
                                    if (trys == 4){
                                        resultMutableList = Judge().addStarsIfNotSolved(turn,resultMutableList)
                                        println("Has perdut")
                                        star = "☆"
                                    }
                                    else if (formatedAnswer == questions[turn-1].correct_answer){
                                        resultMutableList = Judge().addStarsIfSolved(turn,resultMutableList)
                                        goodAnswer++
                                        state = true
                                        star = "★"
                                    }
                                } else{
                                    resultMutableList = Judge().addStarsIfNotSolved(turn,resultMutableList)
                                    star = "☆"
                                    badAnswer = true
                                }
                                ConsoleInputs().statsProblema(turn, state, trys)
                                notPlayedYet = true
                                //turn++
                                println("A continuació s'enviará el següent problema, estas d'acord?"+reset)
                                println(" ▶ Escriu " + greenColor + "1" + reset + " per continuar ")
                                println(" ▶ Escriu " + greenColor + "2" + reset + " per acabar aquí i guardar ")
                                print(inputIGN + " ➜ ")
                                val nextQuestion = scan.nextInt()
                                var points:Int
                                if (nextQuestion == 1){
                                    if (badAnswer){
                                        points = 0
                                        badAnswer = true
                                    }else{
                                        points = Judge().puntuation(trys)
                                    }
                                    userDAO.updateUserProblem(turn,points,star)
                                }else{
                                    points = 0
                                }
                                if (nextQuestion == 2){
                                    userDAO.updateUserProblem(turn,points,star)
                                    notPlayedYet = true
                                    game = false
                                }
                                if(turn==5){
                                    println()
                                    println(greenColor+"HAS ACABAT! \uD83C\uDF89 \uD83C\uDF89"+reset)
                                    println(greenColor+"Espero que t'hagis divertit!")
                                    game = false
                                    println()
                                    println(reset + "Vols tornar a jugar? Escriu SI o NO")
                                    val inputContinueOrNot = scan.next().uppercase()
                                    if (inputContinueOrNot == "NO"){
                                        exitProcess(1)
                                    }
                                    else if (inputContinueOrNot == "SI"){
                                    }
                                }
                            }
                        }
                        5 ->{
                            exitProcess(1)
                        }
                    }
                }while (inputActionPanel != 1 && inputActionPanel != 2 && inputActionPanel != 3 && inputActionPanel != 4 && inputActionPanel != 5)
            }
        }
    }
}