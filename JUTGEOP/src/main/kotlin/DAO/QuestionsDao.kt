package DAO

import Model.Question
import Model.QuestionsDataUser
import java.sql.SQLException

class QuestionsDao : QuestionsDaoInterface {

    val connection = DAO.createConnection()!!

    override fun getAllQuestions(): MutableList<Question>?{
        val selectQueryQuestions = "SELECT * FROM questions"
        val questions = mutableListOf<Question>()
        try {
            //QUERY QUESTIONS
            var preparedStatementQuestions = connection.prepareStatement(selectQueryQuestions)
            var resultQuestions = preparedStatementQuestions!!.executeQuery()

            while(resultQuestions.next()){
                val id = resultQuestions.getInt("id")
                val question = resultQuestions.getString("question")
                val question_help1 = resultQuestions.getString("question_help1")
                val question_help2 = resultQuestions.getString("question_help2")
                val input_question = resultQuestions.getString("input_question")
                val pastebin_link = resultQuestions.getString("pastebin_link")
                val correct_answer = resultQuestions.getString("correct_answer")
                //constructing a Question object and putting data into the list
                questions.add(Question(id, question, question_help1, question_help2, input_question, pastebin_link, correct_answer))
            }
            resultQuestions.close()
            preparedStatementQuestions.close()
        }catch (e: SQLException) {
            println("Error al obtenir les dades de les preguntes: ${e.message}")
        }
        return questions.ifEmpty { null }
    }

    override fun getAllQuestionsDataFromUser(inputIGN:String) : MutableList<QuestionsDataUser>?{
        val questionsDataUsers = mutableListOf<QuestionsDataUser>()
        val selectQueryQuestionsDataUsers = "SELECT * FROM questionsDataUsers where name=?"
        try {

            //QUERY QUESTIONSDATA
            var preparedStatementQuestionsDataUsers = connection.prepareStatement(selectQueryQuestionsDataUsers)
            preparedStatementQuestionsDataUsers.setString(1,inputIGN)
            var resultQuestionsDataUsers = preparedStatementQuestionsDataUsers!!.executeQuery()



            while(resultQuestionsDataUsers.next()){
                val number_question = resultQuestionsDataUsers.getInt("number_question")
                val estate_question = resultQuestionsDataUsers.getString("estate_question")
                val points_question = resultQuestionsDataUsers.getInt("points_question")
                val user_id = resultQuestionsDataUsers.getInt("user_id")
                //constructing a QuestionDataUser object and putting data into the list
                questionsDataUsers.add(QuestionsDataUser(number_question, estate_question, points_question, user_id))
            }
            preparedStatementQuestionsDataUsers.close()
        }catch (e: SQLException) {
            println("Error al obtenir les de les dades de les preguntes del usuari: ${e.message}")
        }
        return questionsDataUsers.ifEmpty { null }
    }

    override fun insertQuestionDataFromUsers(){
        val insertDataQuestionSQL = "INSERT INTO questionsDataUsers (number_question,intents, estate_question, points_question, user_id) VALUES (?,?,?,?,?)"

        try{
            var insertDataQuestionPrepared = connection.prepareStatement(insertDataQuestionSQL)
            var inserDataQuestionsExecutable = insertDataQuestionPrepared!!.executeQuery()

        }catch (e: SQLException) {
            println("Error al inserir les dades de les preguntes: ${e.message}")
        }
    }

    override fun deleteAllQuestions() {
        val deleteQuery = "DELETE * FROM questions"
        try {
            val statement = DAO.connection!!.prepareStatement(deleteQuery)
            statement.executeUpdate()
            statement.close()
        } catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
        }
    }
}