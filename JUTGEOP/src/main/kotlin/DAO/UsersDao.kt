package DAO

import Model.Question
import Model.QuestionsDataUser
import Model.User
import java.sql.SQLException

class UsersDao : UsersDaoInterface {

    override fun getAllUsers ():MutableList<User>?{
        val users = mutableListOf<User>()
        val selectQueryUser = "SELECT * FROM users"
        try {
            //QUERY USER
            var preparedStatementUsers = DAO.connection!!.prepareStatement(selectQueryUser)
            var resultUsers = preparedStatementUsers!!.executeQuery()

            while (resultUsers.next()) {
                val id = resultUsers.getInt("id")
                val name = resultUsers.getString("name")
                val password = resultUsers.getString("password")
                //constructing a User object and putting data into the list
                users.add(User(id, name, password))
            }
            preparedStatementUsers.close()
        }catch (e: SQLException) {
            println("Error al obtenir les dels usuaris: ${e.message}")
        }
        return users.ifEmpty { null }
    }

    override fun addNewUser(name: String, password: String, id: Int) {
        val insertQueryUser = "INSERT INTO users (id, name, password) VALUES (?,?,?)"
        val statement = DAO.connection!!.prepareStatement(insertQueryUser)
        try {
            statement.setInt(1, id)
            statement.setString(2, name)
            statement.setString(3, password)

            statement.executeUpdate()

            statement.close()
        } catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
        }
    }

    fun updateUserProblem( turn: Int, points: Int, star: String) {
            val query = "UPDATE questionsDataUsers SET estate_question=?, points_question=? WHERE number_question=?"
            try {
                val statement = DAO.connection!!.prepareStatement(query)
                statement.setString(1, star)
                statement.setInt(2, points)
                statement.setInt(3, turn)
                statement.executeUpdate()

                statement.close()
            }catch (e: SQLException) {
                println("Error ${e.errorCode}: ${e.message}")
            }
    }
}