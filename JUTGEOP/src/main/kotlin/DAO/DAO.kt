package DAO

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

interface DAO {
    companion object{
        //BD variables
        val jdbcUrl = "jdbc:postgresql://localhost:5432/juez"
        val username = "postgres"
        val password = "postgres"
        var connection: Connection? = null

        fun createConnection() : Connection? {
            //CONNECTION TO DB
            try {
                if (connection == null) connection = DriverManager.getConnection(jdbcUrl, username, password)
            } catch (e: SQLException) {
                println("[ERROR] Connection to DB failing | Error Code:${e.errorCode}: ${e.message}")
            }
            return connection
        }
    }
}