package DAO

import Model.Question
import Model.QuestionsDataUser
import Model.Teacher
import java.sql.SQLException

class TeachersDao : TeachersDaoInterface{

    override fun getAllTeachers(): MutableList<Teacher>? {
        val teachers = mutableListOf<Teacher>()
        val selectQueryTeacher = "SELECT * FROM teachers"
        try {
            //QUERY TEACHERS
            var preparedStatementTeachers = DAO.connection!!.prepareStatement(selectQueryTeacher)
            var resultTeachers = preparedStatementTeachers!!.executeQuery()

            while(resultTeachers.next()){
                val id = resultTeachers.getInt("id")
                val name = resultTeachers.getString("name")
                val password = resultTeachers.getString("password")
                //constructing a Teacher object and putting data into the list
                teachers.add(Teacher(id, name, password))
            }
            preparedStatementTeachers.close()
        }catch (e: SQLException) {
            println("Error al obtenir les dades dels professors: ${e.message}")
        }
        return teachers.ifEmpty { null }
    }

    override fun insertNewQuestions(questionID:Int, question1: String, question2: String, question3: String, question4: String, question5: String, question6: String ){
        val insertNewQuestions = "INSERT INTO questions (id, question, question_help1, question_help2, input_question, pastebin_link, correct_answer) VALUES (?, ?, ?, ?, ?, ? ,?)"
        val queryInsertNewValues = DAO.connection!!.prepareStatement(insertNewQuestions)
        try {
            queryInsertNewValues.setInt(1, questionID)
            queryInsertNewValues.setString(2, question1)
            queryInsertNewValues.setString(3, question2)
            queryInsertNewValues.setString(4, question3)
            queryInsertNewValues.setString(5, question4)
            queryInsertNewValues.setString(6, question5)
            queryInsertNewValues.setString(7, question6)

            queryInsertNewValues.executeUpdate()
            queryInsertNewValues.close()
        } catch (e: SQLException) {
            println("Error al insertar les dades: ${e.message}")
        }

    }

    override fun getReportByName(name: String): MutableList<QuestionsDataUser>? {
        val selectByName = "SELECT * FROM questionsDataUsers WHERE user_id=? ORDER BY number_question"
        val questionsDataUsers = mutableListOf<QuestionsDataUser>()
        try {
            val statement = DAO.connection!!.prepareStatement(selectByName)
            statement.setString(1, name)
            val resultQuestionsDataUsers = statement.executeQuery()

            while(resultQuestionsDataUsers.next()) {
                val number_question = resultQuestionsDataUsers.getInt("number_question")
                val estate_question = resultQuestionsDataUsers.getString("estate_question")
                val points_question = resultQuestionsDataUsers.getInt("points_question")
                val user_id = resultQuestionsDataUsers.getInt("user_id")
                //constructing a Question object and putting data into the list
                questionsDataUsers.add(QuestionsDataUser(number_question, estate_question, points_question, user_id))
            }
            statement.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
        }
        return questionsDataUsers.ifEmpty { null }
    }
}