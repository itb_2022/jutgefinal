package DAO

import Model.Question
import Model.QuestionsDataUser

interface QuestionsDaoInterface: DAO {
    fun getAllQuestions(): MutableList<Question>?
    fun getAllQuestionsDataFromUser(inputIGN:String): MutableList<QuestionsDataUser>?
    fun insertQuestionDataFromUsers()
    fun deleteAllQuestions()
}