package DAO

import Model.User

interface UsersDaoInterface: DAO {
    fun getAllUsers():MutableList<User>?
    fun addNewUser(name:String, password:String, id: Int)
}