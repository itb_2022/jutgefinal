package DAO

import Model.Question
import Model.QuestionsDataUser
import Model.Teacher

interface TeachersDaoInterface: DAO {
    fun getAllTeachers():MutableList<Teacher>?
    fun insertNewQuestions(questionID:Int, question1: String, question2: String, question3: String, question4: String, question5: String, question6: String)
    fun getReportByName(name:String):MutableList<QuestionsDataUser>?
}