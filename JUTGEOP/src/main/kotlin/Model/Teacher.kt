package Model

import DAO.TeachersDao
import UI.ConsoleInputs
import reset
import java.util.*

class Teacher(val id:Int, val name:String, val password:String) {
    var teachersDao = TeachersDao()

    fun addProblem(scan: Scanner, inputIGN:String){
        println(" Escriu el número de la pregunta"+ reset)
        print(inputIGN + " ➜ ")
        val inputTeacherNumberQuestion = scan.nextInt()

        println(" Escriu la pregunta/continuació de la historia"+ reset)
        print(inputIGN + " ➜ ")
        val inputTeacherQuestion = scan.next()
        println(" Escriu el primer enunciat d'ajuda"+ reset)
        print(inputIGN + " ➜ ")
        val inputTeacherHelp1 = scan.next()
        println(" Escriu el segon enunciat d'ajuda"+ reset)
        print(inputIGN + " ➜ ")
        val inputTeacherHelp2 = scan.next()

        println(" Escriu el segon enunciat d'ajuda"+ reset)
        print(inputIGN + " ➜ ")
        val inputTeacherCorrectAnswer = scan.next()
        println(" Escriu l'input y output"+ reset)
        print(inputIGN + " ➜ ")
        val inputTeacherInputOutput = scan.next()
        println(" Escriu el primer enunciat d'ajuda"+ reset)
        print(inputIGN + " ➜ ")
        val inputTeacherPastebin = scan.next()

        teachersDao.insertNewQuestions(inputTeacherNumberQuestion,inputTeacherQuestion,inputTeacherHelp1,inputTeacherHelp2,inputTeacherCorrectAnswer,inputTeacherInputOutput,inputTeacherPastebin)
    }

    fun alumReport(name:String, questions: MutableList<Question>){
        val questionsDataAlum = teachersDao.getReportByName(name)

        ConsoleInputs().panelHistoric(questions, questionsDataAlum!!)
    }
}