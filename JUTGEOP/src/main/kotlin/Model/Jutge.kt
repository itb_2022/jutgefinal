package Model

import DAO.QuestionsDao
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class Judge {
    var questionsDao = QuestionsDao()

    fun formatAnswer(string: String): String {
        // Set var to detect duplicate chars
        var prevChar = ' '
        return string.trim() // Remove surrounding spaces to simplify
            .replace(' ', '_') // Spaces into underscores
            .filter { char -> // Remove duplicated underscores
                if (char == '_' && prevChar == '_') false
                else {
                    prevChar = char
                    true
                }
            }
    }

    private fun factorial(num:Int): Int{
        val num = num
        var factorial = 1
        for (i in 1..num) {
            factorial *= i
        }
        return factorial
    }

    fun puntuation(trys: Int):Int{
        var basePoints = 100
        when (trys){
            1 -> basePoints *= factorial(4)
            2 -> basePoints *= factorial(3)
            3 -> basePoints *= factorial(2)
            4 -> basePoints *= factorial(1)
        }
        return basePoints
    }

    @Suppress("DEPRECATION")
    fun updateUserData(){
        val file = File("file.txt")
        val tempFile = createTempFile()
        val regex = Regex("""some line in file""")
        tempFile.printWriter().use { writer ->
            file.forEachLine { line ->
                writer.println(when {
                    regex.matches(line) -> line + "a some text I'd like to add after line"
                    else -> line
                })
            }
        }
        check(file.delete() && tempFile.renameTo(file)) { "failed to replace file" }
    }

    fun addStarsIfSolved(turn: Int,resultMutableList: MutableList<String>):MutableList<String>{
        when(turn){
            1 -> {resultMutableList.add(0,"★") }
            2 -> {resultMutableList.add(1,"★") }
            3 -> {resultMutableList.add(2,"★") }
            4 -> {resultMutableList.add(3,"★") }
            5 -> {resultMutableList.add(4,"★") }
        }
        return resultMutableList
    }

    fun addStarsIfNotSolved(turn: Int,resultMutableList: MutableList<String>):MutableList<String>{
        when(turn){
            1 -> {resultMutableList.add(0,"☆") }
            2 -> {resultMutableList.add(1,"☆") }
            3 -> {resultMutableList.add(2,"☆") }
            4 -> {resultMutableList.add(3,"☆") }
            5 -> {resultMutableList.add(4,"☆") }
        }
        return resultMutableList
    }

}