package UI

import Model.Question
import Model.QuestionsDataUser

class ConsoleInputs {

    val redColor = "\u001b[31m"
    val yellowColor = "\u001B[33m"
    val greenColor = "\u001B[32m"
    val blueColor = "\u001B[36m"
    val reset = "\u001b[0m"
    val softGreenColor = "\u001B[38;5;70m"

    fun initialPanel(){
        println()
        println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
        println("┃              PANELL INICIAL             ┃")
        println("┃                                         ┃")
        println("┃        1 -> LLISTA DE PROBLEMES         ┃")
        println("┃        2 -> HISTÒRIC PROBLEMES          ┃")
        println("┃        3 -> AJUDA                       ┃")
        println("┃        4 -> JUGAR                       ┃")
        println("┃        5 -> SORTIR                      ┃")
        println("┃                                         ┃")
        println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
        println()
    }
    fun teacherPanel(nameSavedOnDB: String){
        println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
        println("         PANELL PROFESSOR [$nameSavedOnDB]")
        println("                                         ")
        println("        1 -> AFEGIR PROBLEMA             ")
        println("        2 -> REPORT ALUMNE               ")
        println("                                         ")
        println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
        println()
    }
    fun menuPanel(){
        println()
        println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
        println("┃              PANELL D'AJUDA             ┃")
        println("┃                                         ┃")
        println("┃      Aquest joc és una secuència de 5   ┃")
        println("┃   preguntes, les quals presentaràn una  ┃")
        println("┃             serie de problemes.         ┃")
        println("┃                                         ┃")
        println("┃        L'usuari, mitjançant unes        ┃")
        println("┃     dades que se l'hi donaràn, haurà    ┃")
        println("┃   d'introduir el resultat a la màquina. ┃")
        println("┃                                         ┃")
        println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
        println()
    }
    fun panelQuestions(){
        println()
        println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
        println("┃             "+greenColor+"PANELL PREGUNTES"+reset+"            ┃")
        println("┃                                         ┃")
        println("┃    "+greenColor+"Quina pregunta vols visualitzar?"+reset+"     ┃")
        println("┃    "+redColor+"1"+greenColor+" ->"+reset+" Seleccionar jugadors del equip  ┃")
        println("┃    "+redColor+"2"+greenColor+" ->"+reset+" Ordenar jugadors del equip      ┃")
        println("┃    "+redColor+"3"+greenColor+" ->"+reset+" Seleccionar hotel disponible    ┃")
        println("┃    "+redColor+"4"+greenColor+" ->"+reset+" Contador automatic              ┃")
        println("┃    "+redColor+"5"+greenColor+" ->"+reset+" Calories consumides             ┃")
        println("┃                                         ┃")
        println("┃    "+redColor+"9"+greenColor+" ->"+reset+" Sortir                          ┃")
        println("┃                                         ┃")
        println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
    }
    fun panelHistoric(questions: MutableList<Question>, questionsDataUser: MutableList<QuestionsDataUser>){
        println()
        println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
        println("                    PANELL HISTÒRIC            ")
        println("                                         ")
        println("      Aquí tens l'historic de preguntes   ")
        println("      1r ${questions[0].id} ->  ${softGreenColor}Estat:${reset} ${questionsDataUser[0].estate_question} ${softGreenColor}Points:${reset} ${questionsDataUser[0].estate_question} ")
        println("      2n ${questions[1].id} ->  ${softGreenColor}Estat:${reset} ${questionsDataUser[1].estate_question} ${softGreenColor}Points:${reset} ${questionsDataUser[1].estate_question} ")
        println("      3r ${questions[2].id} ->  ${softGreenColor}Estat:${reset} ${questionsDataUser[2].estate_question} ${softGreenColor}Points:${reset} ${questionsDataUser[2].estate_question} ")
        println("      4t ${questions[3].id} ->  ${softGreenColor}Estat:${reset} ${questionsDataUser[3].estate_question} ${softGreenColor}Points:${reset} ${questionsDataUser[3].estate_question} ")
        println("      5é ${questions[4].id} ->  ${softGreenColor}Estat:${reset} ${questionsDataUser[4].estate_question} ${softGreenColor}Points:${reset} ${questionsDataUser[4].estate_question} ")
        println("                                         ")
        println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
    }
    private fun estatePanel(questionsDataUser: MutableList<QuestionsDataUser>, questionNumber: Int){
        if (questionsDataUser[questionNumber].estate_question == null) "❓" else questionsDataUser[questionNumber].estate_question
    }
    private fun pointsPanel(questionsDataUser: MutableList<QuestionsDataUser>, questionNumber: Int){
        if (questionsDataUser[questionNumber].points_question == null) "❓" else questionsDataUser[questionNumber].points_question
    }

    fun pregunta() {
        println("                                $redColor┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓$yellowColor                                                         _____________$reset")
        println("                                "+redColor+"┃          "+reset+"VOLS RESOLDRE"+redColor+"             ┃       "+reset+"▶ Introdueix '"+greenColor+"1"+reset+"' per resoldre'l" +yellowColor+ "                 _/_| [][][][][]|"+reset)
        println("                                "+redColor+"┃         "+reset+"AQUEST PROBLEMA?"+redColor+"           ┃       "+reset+"▶ Introdueix '"+greenColor+"2"+reset+"' per passar al següent" +yellowColor+ "        ☾                |"+reset)
        println("                                $redColor┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛$yellowColor                                                       ￣OO￣￣￣￣￣OO￣=°°°$reset")
    }
    fun problema(id:Int, questions:MutableList<Question>){
        println("$redColor ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓$reset")
        println("    ${questions[id].question}")
        println()
        println("    ${questions[id].question_help1}")
        println()
        println("    ${questions[id].question_help2}")
        println("$redColor ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛$reset")
    }
    fun infoProblemes(id:Int,questions:MutableList<Question>){
        println("    ${questions[id].input_question}")
        println()
        println(reset+"\uD83D\uDD17 Llista:${questions[id].pastebin_link}")
        println()
    }
    fun statsProblema(turn:Int, state:Boolean, trys:Int){
        val stateMessage: String
        if (state){ stateMessage = softGreenColor+"RESOLT" } else stateMessage = redColor+"NO RESOLT"
        println()
        println("                                                                      $redColor┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓")
        println("                                                                      $redColor┃            "+greenColor+"STATS PROBLEMA "+redColor+"#$turn            ┃")
        println("                                                                      $redColor┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛")
        println("                                                                    $reset       > INTENTS -> $trys")
        println("                                                                    $reset       > ESTAT   -> $stateMessage$reset")
        println()
    }
}