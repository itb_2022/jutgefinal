CREATE DATABASE juez;

\c juez;

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name VARCHAR(20) NOT NULL, 
    password VARCHAR(50) NOT NULL
);

CREATE TABLE questionsDataUsers (
    number_question INTEGER,
    estate_question VARCHAR(15),
    points_question INTEGER,
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE teachers (
    id SERIAL PRIMARY KEY,
    name VARCHAR(20) NOT NULL, 
    password VARCHAR(50) NOT NULL
);

CREATE TABLE questions (
    id SERIAL PRIMARY KEY,
    question TEXT NOT NULL,
    question_help1 TEXT NOT NULL,
    question_help2 TEXT NOT NULL,
    input_question TEXT NOT NULL,
    pastebin_link TEXT NOT NULL,
    correct_answer TEXT NOT NULL
);
