INSERT INTO questions (id,question,question_help1,question_help2,input_question,pastebin_link,correct_answer)
VALUES 
(1, 'Estas viajando en el bus camino al torneo TIMAndorra (Torneo de basketball) junto con tus compañeros de equipo, cuánto de repente no están hechas las fichas para poder jugar el primer partido.', 
'El entrenador o el delegado tendrá que hacerlas durante el viaje (lo más seguro es que se marice), te ha pedido ayuda a ti, ya que necesita que le dés los datos de los jugadores de tu equipo, de la lista de nombres de todo el torneo.', 
'Además, es bastante exigente y te ha dicho que sigas el formato dado en el ejemplo siguiente.(Recuerda que tu equipo es el Valldemia).',
'Castillo Perez,Pol,Boet
Moreno Martinez,Joan,Valldemia
Gómez Romero,Jan,UEM
Moreno Martinez,Pau,Valldemia                  Equip:Valldemia J1:Joan,Moreno,Martinez J2:Pau,Moreno,Martinez J3:Victor,Redrado,Pérez J4:Joan,Ribes,Prats
Redrado Pérez,Victor,Valldemia
Sanz Cruz,Jose,Boet
Ribes Prats,Joan,Valldemia
Tabares Galinsoga,Pep,UEM',
'https://pastebin.com/E3JX59B3',
'Equip:Valldemia_J1:Fausto,Cuéllar,Cerdán_J2:Lalo,Arrieta,Prats_J3:Joan,Moreno,Martinez_J4:Pau,Moreno,Martinez_J5:Pascual,Alegre,Quesada_J6:Josep,Casals,Somoza_J7:Victor,Redrado,Pérez_J8:Gervasio,Criado,Rico_J9:Tomás,Pons,Sevilla_J10:Pol,Bernabé,Olmo
',
), 

(2, 'Después de ayudar al staff de tu equipo a conseguir la lista de jugadores, se han dado cuenta de que todavía les faltan unos datos para las fichas, por eso, al haber realizado un trabajo tan bueno, te han pedido que les vuelvas a ayudar.', 
'Te han dado una lista de datos (de nuevo sin ordenar), para que te ayude a formatear correctamente la lista anterior de jugadores. Con la lista de datos de ayuda, tendrás que ordenar la lista de jugadores según su número (según el nombre y el primer apellido se te dará una lista de números).', 
'¡Atención! La lista de números está desactualizada y contiene números de otros años, por eso deberás comprobar si el jugador está en el torneo.',
'23:Victor Redrado
69:Joan Ribes
3:Pau Moreno
1:Joel Martinez                    Joel,Martinez->1 Pau,Moreno->3 Victor,Redrado->23 Jan,Saez->45 Joan,Ribes->69 Joan,Roig->76 Victor,Paez->86 Pol,Molina->94
86:Victor Paez
94:Pol Molina
76:Joan Roig
45:Jan Saez"',
'https://pastebin.com/PgDL6bWi',
'Joel,Martinez->1_Pau,Moreno->3_Ot,Soler->5_David,Sanchez->11_Victor,Redrado->23_Pol,Perez->32_Jan,Saez->45_Robert,Gisber->54_Joan,Ribes->69_Gonzalo,Atienza->73_Joan,Roig->76_Victor,Paez->86_Pol,Molina->94'
), 

(3, 'Parece que finalmente podremos jugar, ya que el staff ha hecho su trabajo y ya tiene las fichas del equipo. Después de un rato por Andorra, parece que ha surgido un nuevo problema, Olga Jubany (La Directora de Valldemia) se le olvidó hacer la reserva del hotel, además, no sabemos si los hoteles tienen las plazas necesarias requeridas para alojar un equipo de basket tan grande (23 personas)',
'Olga nos ha enviado una recopilación de hoteles, junto con sus plazas para ver cuál es el idóneo para reservar. Te han pedido (como siempre...) que crees una función, que según las necesidades, haga una criba de los hoteles.',
'Hotel Montblanc,53 habitacions disponibles(35 dobles)
Hotel Sant Eloi,21 habitacions disponibles(16 dobles)
Hotel Pyrénées,8 habitacions disponibles(2 dobles)             Hotel Sant Eloi,16 Habitacions Dobles,4 restants
Hotel Sol-Park,34 habitacions disponibles(30 dobles)
Hotel Guillem & Spa,74 habitacions disponibles(11 dobles)',
'https://pastebin.com/KMzP3Wha',
'Hotel_Robert,32_Habitacions_Dobles,18_restants'
), 

(4, 'Antes del partido, la delagada de pista te ha dicho que es bastante complicado ir llevando la cuenta de puntos y faltas del equipo para asegurarse de que el staff del torneo no tiene errores, además, en la delaga le gustaría llevar la cuenta de jugadores que han jugado o no el partido', 
'Te han pedido que hagas un pequeño programa automatizado para ir poniendo todo lo que necesitan (puntos, faltas y si ha jugado o no cada jugador del equipo)', 
'No comenta en que trabajes gratis, te ha dicho que debes cumplir ciertos requisitos, deben estar ordenados los jugadores de mayor a menor número de dorsal y además debes tener en cuenta que si ha ido al banquillo el jugador o bien ha salido de ella, deberás marcarlo de la manera específica que te han pedido (podrás ver en el input/output).',
'3:Pau Moreno,3 punts
69:Joan Ribes,2 punts
3:Pau Moreno, 3 punts
3:Pau Moreno, 2 punts                    69:Joan Ribes->5 Punts 3:Pau Moreno->8 Punts 76:Joan Roig->1 Punt 86:Victor Paez->NO HA JUGAT 45:Jan Saez->NO HA JUGAT
86:Victor Paez, NO HA JUGAT
69:Joan Ribes,3 punts
76:Joan Roig,1 punt
45:Jan Saez, NO HA JUGAT',
'https://pastebin.com/fWMn9Udh',
'94:Pol_Molina->NO_HA_JUGAT_86:Victor_Paez->20_76:Joan_Roig->5_69:Joan_Ribes->16_45:Jan_Saez->4_3:Pau_Moreno->13_1:Joel_Martinez->12'
), 

(5, '¡Ha ganado el torneo! Pero no todo iba a ser buena suerte, habéis tenido otro problema para volver, después de esta aventura toca revisión y te han pedido que un papel con todas las calorías que ha comido cada jugador , las sumis y les entregues', 
'Cuidado! La lista esta desordenada, es decir, se han ido apuntando a manija que comía cada jugador, por tanto, deberás organizarlo según las calorías totales, de mayor a menor',
'¡Atención! A última hora te han dicho que hay algunos datos que son las calorías (gramos, proteína, hidratos...) y además te han dicho que los números primos tampoco son calorías.',
'Victor Redrado,345 calories
Joan Roig, 512 calories 
Pau Moreno, 317 calories
Pau Moreno, 876 calories                    Victor Redrado=1003,Pau Moreno=876,Joan Roig=1154,Victor Paez=0,Jan Saez=996
Joan Roig, 642 calories
Victor Paez, 769 calories
Victor Redrado,658 calories
Jan Saez, 996',
'https://pastebin.com/kfmq6Nca',
'Victor_Redrado=2668,Pol_Molina=2448,Jan_Saez=2404,Joan_Ribes=1859,Joan_Roig=1783,Joel_Martinez=1446,Pau_Moreno=876,Victor_Paez=0'
);


INSERT INTO users (id,name,password)
VALUES
(1,
 jutge,
 jutge
);

